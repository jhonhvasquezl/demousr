app.controller('appController', function ($scope, $http) {
	$scope.usuarios = h.get('http:8080/api/user/getUsers').then(function (data){
		s.comentarios = data.data.items;
		},function (error){
	   });

})

app.controller('infoController', function infoController($scope,$routeParams){
	$scope.usuario = $scope.usuarios[$routeParams.id];
})
app.controller('addController', function addController($scope,$location){
	$scope.textButton = 'Añadir Usuario';
	$scope.usuario = {};
	$scope.nuevoUsuario = function(){
		$scope.usuarios.push($scope.usuario);
		$location.url('/');
	}
})
app.controller('editController', function editController($scope,$routeParams,$location){
	$scope.textButton = 'Editar Usuario';
	$scope.usuario = $scope.usuarios[$routeParams.id];
	$scope.editarUsuario = function(){
		$scope.usuarios[$routeParams] = $scope.usuario;
		$location.url('/');
	}
})
app.controller('removeController', function removeController($scope,$routeParams,$location){
	$scope.usuario = $scope.usuarios[$routeParams.id];
	$scope.removerUsuario = function(){
		$scope.usuarios.splice($routeParams.id,1);
		$location.url('/');
	}
})