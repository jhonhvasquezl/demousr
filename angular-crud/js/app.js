var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider.when("/", {
		templateUrl:"acciones/index.html"
	})
	.when('/info/:id', {
		templateUrl: 'acciones/info.html',
		controller: 'infoController'
	})
	.when('/add', {
		title: 'Añadir Usuario',
		templateUrl: 'acciones/add.html',
		controller: 'addController'
	})
	.when('/edit/:id', {
		title: 'Editar Usuario',
		templateUrl: 'acciones/edit.html',
		controller: 'editController'
	})
	.when('/remove/:id', {
		title: 'Elimiar Usuario',
		templateUrl: 'acciones/remove.html',
		controller: 'removeController'
	})
	.otherwise({ redirectTo: '/' })
})
