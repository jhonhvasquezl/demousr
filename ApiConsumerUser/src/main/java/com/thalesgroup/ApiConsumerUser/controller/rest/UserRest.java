package com.thalesgroup.ApiConsumerUser.controller.rest;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thalesgroup.ApiConsumerUser.model.entities.Root;
import com.thalesgroup.ApiConsumerUser.model.services.UserService;


@RestController
@RequestMapping("/api/user")
public class UserRest {

	@Autowired
    UserService userService;
	
	@RequestMapping(value = "/getUsers")
	public List<Root> getUser() {
		// TODO Auto-generated method stub
        List<Root> usr = userService.getUsers();

		return usr;
	}
	@RequestMapping(value = "/getUser/{id}")
	public  List<Root> getUse(@PathVariable("id") int value) {
		// TODO Auto-generated method stub
		 List<Root> usr = userService.getUser(value);

		return usr;
	}	
}
