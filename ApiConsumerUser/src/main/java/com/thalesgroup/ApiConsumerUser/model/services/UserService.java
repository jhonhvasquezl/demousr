package com.thalesgroup.ApiConsumerUser.model.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.thalesgroup.ApiConsumerUser.model.entities.Root;



@Service
public class UserService {

	
	@Autowired
    RestTemplate restTemplate;
	

	public List<Root> getUsers(){
        ResponseEntity<Root[]> response =
                  restTemplate.getForEntity(
                  "http://dummy.restapiexample.com/api/v1/employees",
                  Root[].class);
        Root[] users = response.getBody();
                List<Root> m = Arrays.asList(users);
        return  m;
    }
	
	public List<Root> getUser(int value){
        ResponseEntity<Root[]> response =
                  restTemplate.getForEntity(
                  "http://dummy.restapiexample.com/api/v1/employee/"+value,
                  Root[].class);
        Root[] users = response.getBody();
        List<Root> m = Arrays.asList(users);
        return  m;
    }
}
