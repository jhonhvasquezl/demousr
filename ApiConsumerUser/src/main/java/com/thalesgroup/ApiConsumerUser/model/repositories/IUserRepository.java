package com.thalesgroup.ApiConsumerUser.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thalesgroup.ApiConsumerUser.model.entities.Root;

@Repository
public interface IUserRepository extends JpaRepository<Root, Integer> {
}